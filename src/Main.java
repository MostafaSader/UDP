import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.Scanner;

/**
 * Created by msade on 4/13/2017.
 */
public class Main {


    public static void main(String[] args) throws Exception {
        if (args[0].equals("server")) {
            UDPServer server = new UDPServer(14725); // Enter Ip connection
            server.setRequestRecived(new UDPServer.onRequestReceived() {
                @Override
                public void requestOccur(DatagramPacket data) {
                    System.out.println("C:" + new String(data.getData(), data.getOffset(), data.getLength()));
                }
            });
            server.start();
            System.out.println("Waitnig for Client");
            while (true) {
                server.sendMessage(new Scanner(System.in).nextLine());
            }
        }
        if (args[0].equals("client")) {
            UDPClient client = new UDPClient("192.168.43.195", 14725); // Enter Server Ip & ip connection
            client.setRequeste(new UDPClient.onRequestRecevied() {
                @Override
                public void request(DatagramPacket data) {
                    System.out.println(new String(data.getData(), data.getOffset(), data.getLength()));
                }
            });
            client.start();
            while (true) {
                client.sendMessage(new Scanner(System.in).nextLine());
            }
        }
    }
}

