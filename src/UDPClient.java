//package sample;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * Created by msade on 4/13/2017.
 */
public class UDPClient extends Thread {

    private DatagramSocket clientSocket;
    private InetAddress serverAddress;
    private byte[] sendData = new byte[1024];
    private byte[] reciveData = new byte[1024];
    private int port;
    private onRequestRecevied request;

    public UDPClient(String ip, int port) throws Exception {
        this.port = port;
        clientSocket = new DatagramSocket(port);
        serverAddress = InetAddress.getByName(ip);
    }
    public void sendMessage(String message) throws Exception {
        DatagramPacket sendPacket = new DatagramPacket(message.getBytes(), message.length(), serverAddress, port);
        clientSocket.send(sendPacket);
        System.out.println("Messag sent from Client");
    }
    public void sendData(byte[] arry) throws Exception {
        DatagramPacket sendPacket = new DatagramPacket(arry, arry.length, serverAddress, port);
        clientSocket.send(sendPacket);
    }

    public DatagramPacket recivedPacket() throws IOException {
        DatagramPacket recivePacket = new DatagramPacket(reciveData, reciveData.length, serverAddress, port);
        clientSocket.receive(recivePacket);
        return recivePacket;
    }



    public void setRequeste(onRequestRecevied listener) {
        this.request = listener;
    }

    @Override
    public void run() {
        boolean run = true;
        System.out.println("waiting for server");
        while (run) {
            try {
                DatagramPacket receive = recivedPacket();
                request.request(receive);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public interface onRequestRecevied {
        public void request(DatagramPacket data);
    }
}
