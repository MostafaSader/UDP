import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by msade on 4/13/2017.
 */
public class UDPServer extends Thread{
    private InetAddress serverAddress;
    private byte[] sendData = new byte[1024];
    private byte[] reciveData = new byte[1024];
    private int port;
    private DatagramSocket serverSocket;
    private DatagramPacket clientPacket;
    private onRequestReceived request;


    public UDPServer(int port) throws Exception{
        this.port = port;
        serverSocket = new DatagramSocket(port);
    }

    public UDPServer(int port , onRequestReceived listener){
        this.port = port;
        this.request = listener;
    }

    public void setRequestRecived(onRequestReceived listener){
        this.request = listener;
    }

    public void sendMessage(String message) throws IOException {
        DatagramPacket sendPocket = new DatagramPacket(message.getBytes(),message.length(),clientPacket.getAddress(),port);
        serverSocket.send(sendPocket);
    }
    public void sendDate(byte[] data) throws IOException {
        DatagramPacket sendPacket = new DatagramPacket(data,data.length,clientPacket.getAddress(),port);
        serverSocket.send(sendPacket);
    }
    private DatagramPacket receivePacket() throws IOException {
        DatagramPacket receivePck = new DatagramPacket(reciveData,reciveData.length);
        serverSocket.receive(receivePck);
        clientPacket = receivePck;
        return receivePck;
    }

    @Override
    public void run() {

        boolean run = true;
        while (run) {
            try {

                DatagramPacket receive = receivePacket();
                request.requestOccur(receive);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        System.out.println("end programme");
    }

    public interface onRequestReceived{
        public void requestOccur(DatagramPacket data);
    }

}
